#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

import pygame
from pygame.locals import *
from pygame.sprite import Sprite

class Personaje(Sprite):
	def __init__(self):
		self.image = personaje = pygame.image.load("Imagenes/goku.png").convert_alpha() # Se modificó el tamaño de la imágen de goku 
		self.rect = self.image.get_rect()
		self.rect.move_ip(50, 300)
		self.muerto = 0
	def update(self):
		teclas = pygame.key.get_pressed()
		if teclas[K_a]: #Se modificó la tecla de movimientos, la de Space por la letra (a)
			self.image = personaje = pygame.image.load("Imagenes/gokukamehameha.png").convert_alpha()# Se modifico el diseño de la bala de goku 
		elif kamehameha.rect.x > 860:
			self.image = personaje = pygame.image.load("Imagenes/goku.png").convert_alpha() # Se modifico en tamaño de la imágen de goku 

		if teclas[K_q]:#Se modificó la tecla de movimientos, la de desplazamiento izquierdo  por la letra (q)
			self.image = personaje = pygame.image.load("Imagenes/gokuleft.png").convert_alpha() # Se modifico en tamaño de la imágen de goku 
			if self.rect.x > 0:
				self.rect.x -= 10
		elif teclas[K_l]:#Se modificó la tecla de movimientos, la de desplazamiento derecho por la letra (l)
			self.image = personaje = pygame.image.load("Imagenes/gokuright.png").convert_alpha()# Se modifico en tamaño de la imágen de goku 
			if self.rect.x < 740:
				self.rect.x += 10

		if teclas[K_s]:#Se modificó la tecla de movimientos, la de direccional (subir)  por la letra (s)
			self.image = personaje = pygame.image.load("Imagenes/gokuup.png").convert_alpha()# Se modifico en tamaño de la imágen de goku 
			if self.rect.y > 32:
				self.rect.y -= 10
		elif teclas[K_d]:#Se modificó la tecla de movimientos, la de direccional (bajar) por la letra (d)
			if self.rect.y < 530:
				self.image = personaje = pygame.image.load("Imagenes/gokudown.png").convert_alpha()# Se modificó en tamaño de la imágen de goku 
				self.rect.y += 10

class Kamehameha(Sprite):
	def __init__(self):
		self.image = kamehameha = pygame.image.load("Imagenes/kamehameha.png").convert_alpha()
		self.rect = self.image.get_rect()
		self.rect.move_ip(900, 700)
	def update(self):
		teclas = pygame.key.get_pressed()
		if self.rect.x > 840:
			if teclas[K_a]:
				self.rect.x = (personaje.rect.x + 60)
				self.rect.y = (personaje.rect.y + 14)
		if self.rect.x < 870:
			self.rect.x += 20

class Barravidagoku(Sprite):
	def __init__(self):
		self.image = barravidagoku = pygame.image.load("Imagenes/barravidagoku.png").convert_alpha()
		self.rect = self.image.get_rect()
		self.rect.move_ip(18, 4)
	def update(self):
		if barravidagoku.rect.x <= -152:
			personaje.muerto = 1
		if disparo.rect.y >= (personaje.rect.y - 56):
			if disparo.rect.y <= (personaje.rect.y + 62):
				if disparo.rect.x >= personaje.rect.x:
					if disparo.rect.x <= (personaje.rect.x + 43):
						barravidagoku.rect.x -= 26
						disparo.rect.x = -400
		if minicell.rect.y >= (personaje.rect.y - 56):
			if minicell.rect.y <= (personaje.rect.y + 62):
				if minicell.rect.x >= personaje.rect.x:
					if minicell.rect.x <= (personaje.rect.x + 43):
						barravidagoku.rect.x -= 26
						disparo.rect.x = -400

class Minicell(Sprite):
	def __init__(self):
		self.image = minicell = pygame.image.load("Imagenes/minicell.png").convert_alpha()# Se cambio el personaje el tamaño de la imágen 
		self.rect = self.image.get_rect()
		self.rect.move_ip(650, 300) # Se modificó la posición de la imágen 
		self.bandera = 0
		self.muerto = 0
	def update(self):
		if self.rect.y == 40:
			self.bandera = 0
		elif self.rect.y == 540:
			self.bandera = 1

		if self.bandera == 0:
			self.rect.y += 10
		elif self.bandera == 1:
			self.rect.y -= 10
# Se modificó el nivel del juego 
	def dificil(self):
		if self.rect.x < 0:
			self.rect.x = 400 
		if self.rect.y > 200:
			self.rect.y = 0
		self.rect.x -= 5 
		self.rect.y += 5

class Disparo(Sprite):
	def __init__(self):
		self.image = barravidagoku = pygame.image.load("Imagenes/disparominicell.png").convert_alpha()
		self.rect = self.image.get_rect()
		self.rect.move_ip(-400, -400)
	def update(self):
		if self.rect.x == -400:
			if minicell.rect.y == personaje.rect.y:
				self.rect.x = (minicell.rect.x - 60)
				self.rect.y = (minicell.rect.y - 14)
		if self.rect.x > -400:
			self.rect.x -= 5

class Barravidaminicell(Sprite):
	def __init__(self):
		self.image = barravidaminicell = pygame.image.load("Imagenes/barravidaminicell.png").convert_alpha()
		self.rect = self.image.get_rect()
		self.rect.move_ip(612, 4)
	def update(self):
		if self.rect.x >= 782:
			minicell.muerto = 1
		if kamehameha.rect.y >= minicell.rect.y:
			if kamehameha.rect.y <= (minicell.rect.y + 62):
				if kamehameha.rect.x >= minicell.rect.x:
					if kamehameha.rect.x <= (minicell.rect.x + 43):
						self.rect.x += 6
						kamehameha.rect.x = 900

if __name__ == '__main__':
	# Variables.
	salir = False

	# Establezco la pantalla.
	screen = pygame.display.set_mode((800,600))

	# Establezco el título.
	pygame.display.set_caption("IA enemigo")

	# Creo dos objetos surface.
	fondo = pygame.image.load("Imagenes/namek.png").convert() #Se cambio el diseño del juego (fondo) 
	cuadrovidagoku = pygame.image.load("Imagenes/cuadrovidagoku.png").convert_alpha()
	cuadrovidaminicell = pygame.image.load("Imagenes/cuadrovidaminicell.png").convert_alpha()
	hasperdido = pygame.image.load("Imagenes/Hasperdido.png").convert()# Se modificó el mensaje final de has perdido a GAME OVER
	hasganado = pygame.image.load("Imagenes/Hasganado.jpg").convert()# Se modificó el mensaje final de has gando a VERY GOOD
	# .convert() convierten la superficie a un formato de color que permite imprimirlas mucho mas rápido.

	# Objetos
	temporizador = pygame.time.Clock()
	personaje = Personaje()
	kamehameha = Kamehameha()
	minicell = Minicell()
	disparo = Disparo()
	barravidagoku = Barravidagoku()
	barravidaminicell = Barravidaminicell()

	# Movimiento del personaje.
	while not salir:
		personaje.update()
		kamehameha.update()
		if barravidaminicell.rect.x < 697:
			minicell.update()
		else:
			minicell.dificil()
		disparo.update()
		barravidagoku.update()
		barravidaminicell.update()

		# actualizacion grafica
		screen.blit(fondo, (0, 0))
		screen.blit(personaje.image, personaje.rect)
		screen.blit(kamehameha.image, kamehameha.rect)
		screen.blit(minicell.image, minicell.rect)
		screen.blit(disparo.image, disparo.rect)
		screen.blit(barravidagoku.image, barravidagoku.rect)
		screen.blit(barravidaminicell.image, barravidaminicell.rect)
		screen.blit(cuadrovidagoku, (0,0))
		screen.blit(cuadrovidaminicell, (608,0))
		if personaje.muerto == 1:
			screen.blit(hasperdido, (250,264))
		if minicell.muerto == 1:
			screen.blit(hasganado, (250,264))
		pygame.display.flip()

		if personaje.muerto == 1:
			pygame.time.delay(3000)
			salir = True
			if  minicell.muerto == 1:
				pygame.time.delay(3000)
				salir = True
			temporizador.tick(10) #Se estableión la velocidad del personaje contario (vegueta)
		temporizador.tick(50) #Se modifico la velocidad del personaje principal (goku)

		# gestion de eventos
		for evento in pygame.event.get():
			if evento.type == pygame.QUIT:
				salir = True
